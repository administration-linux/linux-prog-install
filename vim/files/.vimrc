" coding configuration 
set encoding=utf-8
set fileencoding=utf-8

set nocompatible 	" Ignorer la compatibilité avec VI
syntax on       	" Coloriser la syntaxe
filetype plugin on 	" Important pour la navigation dossiers/fichiers

set path+=**    	" Permet de rechercher un fichier récursivement
set wildmenu    	" Afficher les fichiers qui matchent dans un menu

set colorcolumn=80	" Barre verticale
set hlsearch       	" Recherche en surbrillance
set ignorecase     	" Ignore la casse lors d'une recherche
set autoread       	" Rechargement automatique d'un fichier
set noswapfile		" Pas de fichier d'échange
set showcmd		" Afficher les commandes
set number		" Afficher ls numéros de ligne

"
" fermetures automatiques (, [, " et '
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap " ""<Esc>i
inoremap ' ''<Esc>i

"
"" Identation par 4 espaces
set ts=4
set sw=4
set sts=4
set et
set autoindent
filetype plugin indent on


call plug#begin('~/.vim/plugged')
Plug 'tomasr/molokai'
call plug#end()


" colorscheme 
colorscheme molokai 
set background=dark
"

" mapping Shell
inoremap $$ ${}<Esc>i

" mapping C
inoremap #i #include <.h><Esc>2hi
inoremap { <Space>{<CR><CR><Tab><CR>}<Esc>kI<BS>
