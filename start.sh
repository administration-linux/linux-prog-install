#! /bin/bash

# Resources :
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-and-upgrading-ansible-with-pip
# https://forums.linuxmint.com/viewtopic.php?t=362499
# https://unix.stackexchange.com/a/135090

# Check that there is a file in /etc/sudoers.d/ that contains : <user> ALL=(ALL:ALL) NOPASSWD: ALL

user=$(whoami)
echo "> Check ${user} permissions"
if [ -f "/etc/sudoers.d/${user}" ]; then
    sudo grep -q "${user} ALL=(ALL:ALL) NOPASSWD: ALL" /etc/sudoers.d/"${user}"
    if [ ${?} != 0 ]; then
        echo "> File /etc/sudoers.d/${user} should contain ${user} ALL=(ALL:ALL) NOPASSWD: ALL"
        exit 1
    fi
else
    echo "> File /etc/sudoers.d/${user} should exists !"
    exit 1;
fi
echo "> User ${user} permissions OK"

# Remove Python2
# (re)Install Python3
# Create symlink python -> python3
# Install pip

echo "> apt update & upgrade"
sudo apt update > /dev/null 2>&1
sudo apt upgrade -y > /dev/null 2>&1
echo "> Removing python2"
sudo apt remove python2 > /dev/null 2>&1
sudo apt autoremove --purge -y > /dev/null 2>&1
echo "> Install python3"
sudo apt install python3 python3-venv -y > /dev/null 2>&1
echo "> Install python-is-python3"
sudo apt install python-is-python3 -y > /dev/null 2>&1
echo "> Install pipx"
sudo apt install pipx -y > /dev/null 2>&1
pipx ensurepath
sudo pipx ensurepath
#sudo apt install python3-pip -y > /dev/null 2>&1
#python -m pip install --upgrade pip > /dev/null 2>&1
#pip install pexpect > /dev/null 2>&1

# Ansible installation

echo "> Install and/or update Ansible"
pipx install --include-deps ansible
#python -m pip install --user  > /dev/null 2>&1
#python -m pip install --upgrade --user ansible > /dev/null 2>&1

# Create SSH key pair if don't exist
# Do the ssh-copy-id manually

echo "> Generate SSH key pair"
mkdir ~/.ssh
chmod 0700 ~/.ssh
touch ~/.ssh/authorized_keys
chmod 0600 ~/.ssh/authorized_keys
< /dev/zero ssh-keygen -t ed25519 -f ~/.ssh/ansible_ed25519 -q -P "" > /dev/null 2>&1
public_key=$(cat ~/.ssh/ansible_ed25519.pub)
grep -q "${public_key}" ~/.ssh/authorized_keys
if [ ${?} != 0 ]; then
    echo "> Add SSH public key in ~/.ssh/authorized_keys"
    cat ~/.ssh/ansible_ed25519.pub | cat >> ~/.ssh/authorized_keys
fi

# Run ansible script
echo "> Run ansible script"
ubuntu_codename=$(sudo cat /etc/os-release | grep UBUNTU_CODENAME | sed 's/UBUNTU_CODENAME=//')
ansible-playbook main.yml -e remote_user_home="${user}" -e ubuntu_codename="${ubuntu_codename}"
