# Présentation

Script Ansible d'installation et de configuration des programmes sous Linux Mint.

le scrip se lance via la commande : 

```shell
./start.sh
```

# Prérequis

## Avoir les droits en sudo sans mot de passe pour l'`utilisateur`

Dans le répertoire `/etc/sudoers.d/`, créer un fichier `utilisateur`, contenant :

```shell
utilisateur ALL=(ALL:ALL) NOPASSWD: ALL
```

## Installer git

`sudo apt update & sudo apt install git -y`

## Ne pas se trouver derrière un proxy

En gros, lancer ce script à la maison, pas au bureau !

# Actions post script

## Session

Se déconnecter, puis se reconnecter à sa session utilisateur. Ceci afin de :
- ajouter son utilisateur au groupe `docker`.
- ...

## Application 'Relevés du système'

- Aquitter le message "Il manque les paquets [...] firefox-locale-fr"
- ...

## Vim

Lancer Vim, l'erreur suivante apparait :

// TODO

Faire `Entrée` et taper `:PlugInstall` pour installer les plugins.

## Navigateurs (Chromium et Firefox)

### Extensions à installer

- Bitwarden
- Authentificateur
- Raindrop
- DeepL
- uBlock Origin
- SponsorBlock
- CORS Unblock
- Vue.js devtools

## Java

Le script installe les JDK 8, 11, 17 et 21.

[Doc sur les alternatives Java.](https://www.baeldung.com/linux/java-choose-default-version)

```shell
update-java-alternatives --list
java --version
sudo update-java-alternatives --set java-x.y.z-openjdk-amd64
```

ou 

`update-alternatives --config java`

# Bugs

- [à corriger] comment gérer le rechargement du terminal dès l'installation de pipx (repasser à pip (cd. Pascal))
- [à corriger] problème téléchargement Anki car modification url
- [à corriger] impossible d'installer Obsidian, à creuser

